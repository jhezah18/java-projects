/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 2ndyrGroupA
 */
public class Customer {

    private String name;
    private boolean member;
    private String memberType;

    public Customer(String name) {
        this.member = false;
    }

    

    public String getName() {
        return name;
    }

    public boolean isMember() {
        return member;
    }
    public void setMember(boolean member){
        
    }

    public String getMemberType() {
        return memberType;
    }
    

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }


    public String toString() {
        return "Customer{" +
                "name='" + name + '\'' +
                ", member=" + member +
                ", memberType='" + memberType + '\'' +
                '}';
    }
}
